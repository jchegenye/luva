<!DOCTYPE html>
<html lang="en" class="no-js" >
<head>

	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<meta name="description" content="Kenyan born & raised young talented guy with basic knowlege in all the musical instruments & a master of the violin." />
	<meta name="author" content="Jackson Asumu Chegenye" />

	<!-- Open Graph tags to customize link previews. -->
	<link rel="canonical" href="http://luva.j-tech.tech/" />
	<meta property="og:url"           content="http://luva.j-tech.tech/" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="Brian Luvasi" />
	<meta property="og:description"   content="Kenyan born & raised young talented guy with basic knowlege in all the musical instruments & a master of the violin." />
	<meta property="og:image"         content="http://luva.j-tech.tech/assets/img/luva4.png" />
	<!--[if IE]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->
	<title>Brian Luvasi | The Violin Genius</title>
	<link href="assets/css/bootstrap.css" rel="stylesheet" />
	<link href="assets/css/ionicons.css" rel="stylesheet" />
	<link href="assets/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/js/source/jquery.fancybox.css" rel="stylesheet" />
	<link href="assets/css/animations.min.css" rel="stylesheet" />
	<link href="assets/css/style-green.css" rel="stylesheet" />
	<link rel="icon" type="image/x-icon" href="/assets/favicons/favicon.ico">

	<script src="assets/sweetalert-master/dist/sweetalert.min.js"></script> 
	<link rel="stylesheet" type="text/css" href="assets/sweetalert-master/dist/sweetalert.css">
 	
 	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js" type="text/javascript"></script>

</head>

<body>

<!--MENU SECTION-->
<div class="navbar navbar-inverse navbar-fixed-top scroll-me" id="menu-section" >
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">Brian Luvasi</a>

			<!-- Song Credit: David Garrett - Viva La Vida -->
			<audio id="myAudio" loop controls autoplay style="margin-top: 10px;margin-left: 10px; ">
			  <source src="assets/audio/vivaLaVida.mp3" type="audio/mpeg">
			  Your browser does not support the audio element.
			</audio>

			<!-- Background Audio Script -->
			<script>
				var aud = document.getElementById("myAudio");
				aud.onplay = function() {
					swal({   
						title: "Background music alert!",   
						text: "To enjoy my videos, kindly pause the background music above.",
						imageUrl: "assets/favicons/violin-72-159037.png",   
						timer: 10000,   
						showConfirmButton: false 
					});
				};
			</script>

			<!-- Facebook SDK for JavaScript -->
			<div id="fb-root"></div>
			<script>
				window.fbAsyncInit = function() {
				FB.init({appId: '1000499559999755', status: true, cookie: true,
				xfbml: true});
				};
				(function() {
				var e = document.createElement('script'); e.async = true;
				e.src = document.location.protocol +
				'//connect.facebook.net/en_US/all.js';
				document.getElementById('fb-root').appendChild(e);
				}());
			</script>
						
			<script type="text/javascript">
				$(document).ready(function(){
				$('#share_button').click(function(e){
				e.preventDefault();
				FB.ui(
				{
				method: 'feed',
				name: 'Brian Luvasi',
				link: 'http://luva.j-tech.tech/',
				picture: 'http://luva.j-tech.tech/assets/img/luva4.png',
				caption: 'Website',
				description: "Kenyan born & raised young talented guy with basic knowlege in all the musical instruments & a master of the violin.",
				message: ""
				});
				});
				});
			</script>

			<script src="https://apis.google.com/js/platform.js"></script>

			<!-- Youtube SDK for JavaScript -->
			<script>
			  function onYtEvent(payload) {
			    if (payload.eventType == 'subscribe') {
			      // Add code to handle subscribe event.
			    } else if (payload.eventType == 'unsubscribe') {
			      // Add code to handle unsubscribe event.
			    }
			    if (window.console) { // for debugging only
			      window.console.log('YT event: ', payload);
			    }
			  }
			</script>
			<!-- Youtube SDK for JavaScript -->
			<script>(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.src="//x.instagramfollowbutton.com/follow.js";s.parentNode.insertBefore(g,s);}(document,"script"));</script>

		</div>

		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#home">ME</a></li>
				<li><a href="#video" data-toggle="dropdown">VIDEOS
				   <!-- ALBUMS<span class="caret"></span> --></a>
				   	<!-- <ul class="dropdown-menu">
						<li><a href="#audio">Audio</a></li>
						<li><a href="#video">Video</a></li>
				   	</ul> -->
				 </li>
				<li><a href="#events">EVENTS</a></li>
				<li><a href="#gallery">GALLERY</a></li>
				<li><a href="#contact">CONTACT</a></li>
			</ul>
		</div>
	</div>
</div>

<!-- BACKGROUND ALERT -->
	<div class = "alert-box" >
	   	<span id="insertHere"></span>
	</div>

<!-- HOME SECTION -->
<section id="home">

	<div class="container">
		<div class="row">

			<div class="col-sm-8">
				<h1>
					Brian Luvasi 
				</h1>
				<h3>
				 	The Violin Genius  
				</h3>

				<div class="line-under"></div>

				<h5>
					I' am a young Kenyan multi talented guy, with basic knowlege in all the musical instruments & a master of the violin. <br>
					I usually perform solos in 
						<b class="wow fadeInDown"> 
							<span class=element></span>
						</b><br> 
					I also giving <b>Private Lessons</b> to violin enthusiasts. 
				</h5>

				<div class="line-under"></div>

				<p class="social">
					<a href="#" class="btn button-custom btn-custom-one" id = "share_button">
						<i class="fa fa-facebook "></i>
					</a>	
					<a href="#" class="btn button-custom btn-custom-one" ><i class="fa fa-instagram"></i></a>
					<a href="whatsapp://send?text=Hi, kindly visit my website - Brian Luvasi at http://luva.j-tech.tech/" class="btn button-custom btn-custom-one" ><i class="fa fa-whatsapp "></i></a>
					
					<div class="g-ytsubscribe" data-channelid="UCBab5sEtlttNCAvwEMjJ05A" data-layout="full" data-count="default" data-onytevent="onYtEvent"></div>

					
			
				</p>

			</div>

		</div>
	</div>
</section>

<!--VIDEO SECTION START-->
<section id="video" >
	<div class="container">
		<div class="row header">
			<div class="col-md-12 animate-in" data-anim-type="fade-in-up">
				<h3>My Video's</h3>
				<div class="line-under-header"></div>
			</div>
		</div>

		<div class="row animate-in" data-anim-type="fade-in-up" style="margin-top: -25px;">
			<div class="col-sm-12">
				<div class="video-wrapper">
					<div class="animate-in" data-anim-type="fade-in-up">
						<!-- <iframe id="vid_frame" src="http://www.youtube.com/embed/eg6kNoJmzkY?rel=0&showinfo=0&autohide=1" frameborder="0"></iframe> -->
						<iframe width="100%" height="480" src="https://www.youtube-nocookie.com/embed/videoseries?list=PLUwZVmN--jUS4iu1H7i9XUXd8VGg5umbE" frameborder="0" allowfullscreen></iframe>
					</div>
					<div style="padding: 15px;">
						Enjoy my playlist, dont forget to subscribe & Share.
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!--WORK SECTION START-->
<section id="gallery" >
	<div class="container">
		<div class="row header">
			<div class="col-md-12 animate-in" data-anim-type="fade-in-up">
				<h3>My Photo's</h3>
				<div class="line-under-header"></div>
			</div>
		</div>

		<div class="row animate-in" data-anim-type="fade-in-up" >
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pad-bottom">
				<div class="caegories">
					<a href="#" data-filter="*" class="active btn btn-custom btn-custom-two btn-sm">All</a>
					<a href="#" data-filter=".group" class="btn btn-custom btn-custom-two btn-sm">Usoni Band</a>
					<a href="#" data-filter=".personal" class="btn btn-custom btn-custom-two btn-sm">Personal</a>
				</div>
			</div>
		</div>

		<div class="row text-center animate-in" data-anim-type="fade-in-up" id="work-div">
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 group">
				<div class="gallery-wrapper">
					<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/1.jpg">
						<img src="assets/img/portfolio/1.jpg" class="img-responsive img-rounded" alt="" />
					</a>
					<h4>Usoni Band</h4>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 group ">
				<div class="gallery-wrapper">
					<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/2.jpg">
						<img src="assets/img/portfolio/2.jpg" class="img-responsive img-rounded" alt="" />
					</a>
					<h4>Usoni Band</h4>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 group">
				<div class="gallery-wrapper">
					<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/3.jpg">
						<img src="assets/img/portfolio/3.jpg" class="img-responsive img-rounded" alt="" />
					</a>
					<h4>Usoni Band</h4>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 group">
				<div class="gallery-wrapper">
					<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/5.jpg">
						<img src="assets/img/portfolio/5.jpg" class="img-responsive img-rounded" alt="" />
					</a>
					<h4>Usoni Band</h4>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 group">
				<div class="gallery-wrapper">
					<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/13.jpg">
						<img src="assets/img/portfolio/13.jpg" class="img-responsive img-rounded" alt="" />
					</a>
					<h4>Usoni Band</h4>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 group">
				<div class="gallery-wrapper">
					<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/12.jpg">
						<img src="assets/img/portfolio/12.jpg" class="img-responsive img-rounded" alt="" />
					</a>
					<h4>Usoni Band</h4>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 group">
				<div class="gallery-wrapper">
					<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/11.jpg">
						<img src="assets/img/portfolio/11.jpg" class="img-responsive img-rounded" alt="" />
					</a>
					<h4>Usoni Band</h4>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 group">
				<div class="gallery-wrapper">
					<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/8.jpg">
						<img src="assets/img/portfolio/8.jpg" class="img-responsive img-rounded" alt="" />
					</a>
					<h4>Usoni Band</h4>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 personal">
				<div class="gallery-wrapper">
					<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/4.jpg">
						<img src="assets/img/portfolio/4.jpg" class="img-responsive img-rounded" alt="" />
					</a>
					<h4>Brian Luvasi</h4>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 personal">
				<div class="gallery-wrapper">
					<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/6.jpg">
						<img src="assets/img/portfolio/6.jpg" class="img-responsive img-rounded" alt="" />
					</a>
					<h4>Brian Luvasi</h4>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 personal">
				<div class="gallery-wrapper">
					<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/7.jpg">
						<img src="assets/img/portfolio/7.jpg" class="img-responsive img-rounded" alt="" />
					</a>
					<h4>Brian Luvasi</h4>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 personal">
				<div class="gallery-wrapper">
					<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/9.jpg">
						<img src="assets/img/portfolio/9.jpg" class="img-responsive img-rounded" alt="" />
					</a>
					<h4>Brian Luvasi</h4>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 personal">
				<div class="gallery-wrapper">
					<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/10.jpg">
						<img src="assets/img/portfolio/10.jpg" class="img-responsive img-rounded" alt="" />
					</a>
					<h4>Brian Luvasi</h4>
				</div>
			</div>

		</div>
	</div>
</section>

<!--CONTACT SECTION START-->
<section id="contact" >
	<div class="container">
		<div class="row header">
			<div class="col-md-12 animate-in" data-anim-type="fade-in-up">
				<h3>Contact Detail's</h3>
				<div class="line-under-header"></div>
			</div>
		</div>

		<div class="row animate-in" data-anim-type="fade-in-up">

			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<div class="contact-wrapper">
					<h3>Find Me On Social</h3>
					<p>
					Get intouch with me today? I'd Love to hear from you anytime.
					</p>
					<div class="social-below">
						<a href="https://www.facebook.com/The-Violin-Guy-599016803485869/" class="btn button-custom btn-custom-two" target="_blank"> Facebook</a>
						<a href="https://www.instagram.com/luvasi254/" class="btn button-custom btn-custom-two" target="_blank"> Instagram</a>
						<a href="https://www.youtube.com/c/brianluvasi" class="btn button-custom btn-custom-two" target="_blank"> Youtube</a>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<div class="contact-wrapper">
					<h3>Quick Contact</h3>
					<h4><strong>Email : </strong> luvasib@gmail.com </h4>
					<h4><strong>Call : </strong> 
							<br />
							+(254) 724 893 430 
							<br />
							+(254) 735 955 489
					</h4>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<div class="contact-wrapper">
					<h3>About Me : </h3>
					<h4> 
						Kenyan born & raised young talented guy with basic knowlege in all the musical instruments & a master of the violin. 
					</h4>
					<div class="footer-div" >
						<a>2016 &copy;</a> 
						<a href="http://luva.j-tech.tech" target="_blank">luvasib.com</a> 
						| Designed by 
						<a href="http://www.j-tech.tech" target="_blank">jchegenye</a>
					</div>
					<div class="shape photo" style="background: url(assets/images/front-gallery1.jpg);">
            		</div>
				</div>
			</div>

		</div>
	</div>

</section>

<!-- PLAY LIST SECTION -->
<!-- <section id="audio" style="position:relative;">
	<div class="container">
		<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/5442878&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
	</div>
</section> -->
	<script src="assets/js/jquery-1.11.1.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/vegas/jquery.vegas.min.js"></script>
	<script src="assets/js/jquery.easing.min.js"></script>
	<script src="assets/js/source/jquery.fancybox.js"></script>
	<script src="assets/js/jquery.isotope.js"></script>
	<script src="assets/js/appear.min.js"></script>
	<script src="assets/js/animations.min.js"></script>
	<script src="assets/js/custom.js"></script>
	<script src="assets/js/typed.js"></script>
</body>

</html>
